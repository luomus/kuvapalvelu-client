package fi.luomus.kuvapalvelu.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class MetaLegacyTests {

	@Test
	public void test_primary() {
		Meta meta = new Meta();
		assertFalse(meta.isPrimaryForTaxon("MX.123"));

		meta.addTag("primary");
		assertFalse(meta.isPrimaryForTaxon("MX.123"));

		meta.doLegacyConversions();
		assertFalse(meta.isPrimaryForTaxon("MX.123"));
		assertEquals("[]", meta.getPrimaryForTaxon().toString());

		meta.getIdentifications().addTaxonId("MX.123").addTaxonId("MX.456");
		assertFalse(meta.isPrimaryForTaxon("MX.123"));
		assertEquals("[]", meta.getPrimaryForTaxon().toString());

		meta.doLegacyConversions();

		assertTrue(meta.isPrimaryForTaxon("MX.123"));
		assertEquals("[MX.123, MX.456]", meta.getPrimaryForTaxon().toString());
	}

	@Test
	public void test_live() {
		Meta meta = new Meta();
		meta.addTag("live");

		assertEquals(null, meta.getType());

		meta.doLegacyConversions();
		assertEquals("MM.typeEnumLive", meta.getType());
	}

	@Test
	public void test_doesnt_override() {
		Meta meta = new Meta();
		meta.setType("MM.someType");
		meta.addTag("live");
		meta.doLegacyConversions();
		assertEquals("MM.someType", meta.getType());
	}

	@Test
	public void test_lifestage() {
		Meta meta = new Meta();
		meta.addLifeStage("MY.someLifeStage");
		meta.addTag("adult");
		meta.doLegacyConversions();

		assertEquals("[MY.someLifeStage, MY.lifeStageAdult]", meta.getLifeStage().toString());
	}

}
