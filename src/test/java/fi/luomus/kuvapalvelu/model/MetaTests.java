package fi.luomus.kuvapalvelu.model;

import static org.junit.Assert.assertEquals;

import org.joda.time.DateTime;
import org.junit.Test;

import fi.luomus.utils.model.LocalizedString;

public class MetaTests {

	@Test
	public void test_tostring() {
		Meta meta = new Meta();
		meta.setCaption("capt").setCaptureDateTime(DateTime.parse("2020-02-02"))
		.addCapturer("capt1").addCapturer("capt2").addDocumentId("someuri").addDocumentId("http://tun.fi/JA.1")
		.setFullResolutionMediaAvailable(true).setLicense("somelicense").addLifeStage("alive").setOriginalFilename("image.jgp")
		.addPlantLifeStage("seed").addPrimaryForTaxon("MX.1").setRightsOwner("me").setSecret(false).addSex("male").setSortOrder(24)
		.setSourceSystem("KE.1").addTag("keyword").addTag("keyword2").setType("type").setUploadedBy("MA.1").latestVersion();

		LocalizedString locs = new LocalizedString();
		locs.put("fi", "teksti");
		locs.put("en", "text");

		meta.setTaxonDescriptionCaption(locs);

		meta.getIdentifications().addTaxonId("MX.1").addTaxonId("MX.2").addVerbatim("verb").addVerbatim("verb2");

		assertEquals("" +
				"Meta [metadataVersion=v2, sourceSystem=KE.1, uploadDateTime=null, modifiedDateTime=null, license=somelicense, rightsOwner=me, secret=false, capturers=[capt1, capt2], captureDateTime=2020-02-02T00:00:00.000+02:00, uploadedBy=MA.1, modifiedBy=null, originalFilename=image.jgp, documentIds=[someuri, http://tun.fi/JA.1], tags=[keyword, keyword2], identifications=Identifications [taxonIds=[MX.1, MX.2], verbatim=[verb, verb2]], caption=capt, taxonDescriptionCaption={fi=teksti, en=text}, sex=[male], lifeStage=[alive], plantLifeStage=[seed], primaryForTaxon=[MX.1], side=null, type=type, sortOrder=24, fullResolutionMediaAvailable=true]",
				meta.toString());
	}

}
