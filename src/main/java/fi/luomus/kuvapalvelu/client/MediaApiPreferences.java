package fi.luomus.kuvapalvelu.client;

import java.net.URI;

public interface MediaApiPreferences {

	/**
	 * @return The base location of the media api instance
	 */
	URI getBaseUri();
	String getUsername();
	String getPassword();

}
