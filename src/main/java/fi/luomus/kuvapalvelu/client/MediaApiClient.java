package fi.luomus.kuvapalvelu.client;

import java.io.File;
import java.io.InputStream;
import java.util.List;
import java.util.Optional;

import fi.luomus.kuvapalvelu.model.License;
import fi.luomus.kuvapalvelu.model.LifeStage;
import fi.luomus.kuvapalvelu.model.Media;
import fi.luomus.kuvapalvelu.model.MediaClass;
import fi.luomus.kuvapalvelu.model.Meta;
import fi.luomus.kuvapalvelu.model.Sex;
import fi.luomus.kuvapalvelu.model.Side;
import fi.luomus.kuvapalvelu.model.Type;
import fi.luomus.utils.exceptions.ApiException;
import fi.luomus.utils.exceptions.NotFoundException;

public interface MediaApiClient extends AutoCloseable {

	String CLIENT_IDENTIFIER = "IMAGE_API";

	/**
	 * Upload image file and metadata using filename as original file name
	 *
	 * @param file File handle
	 * @param meta Media metadata
	 * @return Created media resource id
	 * @throws ApiException An unexpected error happened while communicating with the API
	 * @throws MediaFormatNotSupported The uploaded file was not in a recognized format
	 */
	String uploadImage(File file, Meta meta) throws ApiException;

	/**
	 * Upload audio file and metadata using filename as original file name
	 *
	 * @param file File handle
	 * @param meta Media metadata
	 * @return Created media resource id
	 * @throws ApiException An unexpected error happened while communicating with the API
	 * @throws MediaFormatNotSupported The uploaded file was not in a recognized format
	 */
	String uploadAudio(File file, Meta meta) throws ApiException;

	/**
	 * Upload PDF file and metadata using filename as original file name
	 *
	 * @param file File handle
	 * @param meta Media metadata
	 * @return Created media resource id
	 * @throws ApiException An unexpected error happened while communicating with the API
	 * @throws MediaFormatNotSupported The uploaded file was not in a recognized format
	 */
	String uploadPdf(File file, Meta meta) throws ApiException;

	/**
	 * Upload image from byte stream using the provided filename and metadata
	 * @param stream The stream
	 * @param filename Original file name
	 * @param meta Media metadata
	 * @return Created media resource id
	 * @throws ApiException An unexpected error happened while communicating with the API
	 * @throws MediaFormatNotSupported The uploaded file was not in a recognized format
	 */
	String uploadImage(InputStream stream, String filename, Meta meta) throws ApiException;

	/**
	 * Upload audio from byte stream using the provided filename and metadata
	 * @param stream The stream
	 * @param filename Original file name
	 * @param meta Media metadata
	 * @return Created media resource id
	 * @throws ApiException An unexpected error happened while communicating with the API
	 * @throws MediaFormatNotSupported The uploaded file was not in a recognized format
	 */
	String uploadAudio(InputStream stream, String filename, Meta meta) throws ApiException;

	/**
	 * Upload pdf from byte stream using the provided filename and metadata
	 * @param stream The stream
	 * @param filename Original file name
	 * @param meta Media metadata
	 * @return Created media resource id
	 * @throws ApiException An unexpected error happened while communicating with the API
	 * @throws MediaFormatNotSupported The uploaded file was not in a recognized format
	 */
	String uploadPdf(InputStream stream, String filename, Meta meta) throws ApiException;

	/**
	 * Upload multiple files as single media entry (for example video + thumbnail or low detaild model + video + thumbnail)
	 * @param mediaClass Media type (of the metadata, ie MM.video, MM.model)
	 * @param sourceFiles Files, filenames and media types
	 * @param meta Media metadata
	 * @return Created media resource id
	 * @throws ApiException An unexpected error happened while communicating with the API
	 * @throws MediaFormatNotSupported The uploaded file was not in a recognized format
	 */
	String upload(MediaClass mediaClass, MultiSourceMedia sourceFiles, Meta meta) throws ApiException;

	/**
	 * Update the metadata of the media resource with the given id.
	 *
	 * @param mediaClass media type
	 * @param id Media resource id
	 * @param newMeta New metadata
	 * @throws ApiException An unexpected error happened while communicating with the API
	 * @throws NotFoundException Thrown if there is no media resource with the given id
	 */
	void update(MediaClass mediaClass, String id, Meta newMeta) throws ApiException, NotFoundException;

	/**
	 * Delete media resource with the given id
	 *
	 * @param mediaClass Media type
	 * @param id Media resource id
	 * @throws ApiException An unexpected error happened while communicating with the API
	 * @throws NotFoundException Thrown if there is no media resource with the given id
	 */
	void delete(MediaClass mediaClass, String id) throws ApiException, NotFoundException;

	/**
	 * Get media resource with the given id
	 *
	 * @param mediaClass Media type
	 * @param id Media resource id
	 * @return The media resource with the given id
	 * @throws ApiException An unexpected error happened while communicating with the API
	 */
	Optional<Media> get(MediaClass mediaClass, String id) throws ApiException;

	/**
	 * Get a list of licenses available for use
	 *
	 * @return All licenses
	 * @throws ApiException An unexpected error happened while communicating with the API
	 */
	List<License> getLicenses() throws ApiException;

	/**
	 * Get a list of sexes available for use (MY.sexes)
	 *
	 * @return All sexes
	 * @throws ApiException An unexpected error happened while communicating with the API
	 */
	List<Sex> getSexes() throws ApiException;

	/**
	 * Get a list of life stages available for use (MY.lifeStages)
	 *
	 * @return All life stages
	 * @throws ApiException An unexpected error happened while communicating with the API
	 */
	List<LifeStage> getLifeStages() throws ApiException;

	/**
	 * Get a list of plant life stages available for use (MY.plantLifeStageEnum)
	 *
	 * @return All plant life stages
	 * @throws ApiException An unexpected error happened while communicating with the API
	 */
	List<LifeStage> getPlantLifeStages() throws ApiException;

	/**
	 * Get a list of sides (upside, downside - MM.sideEnum)
	 *
	 * @return All sides
	 * @throws ApiException An unexpected error happened while communicating with the API
	 */
	List<Side> getSides() throws ApiException;

	/**
	 * Get a list of taxon media types available for use (MM.typeEnum)
	 *
	 * @return All types
	 * @throws ApiException An unexpected error happened while communicating with the API
	 */
	List<Type> getTypes() throws ApiException;

	/**
	 * Retrieve all media matching query parameters
	 *
	 * @param mediaClass Media type
	 * @param fieldName Field name, e.g. "identifications.taxonIds"
	 * @param value Single field value
	 * @return All media that have matching metadata
	 * @throws ApiException An unexpected error happened while communicating with the API
	 */
	List<Media> search(MediaClass mediaClass, String fieldName, String value) throws ApiException;

	/**
	 * Retrieve all media matching query parameters with specified limit and offset
	 *
	 * @param mediaClass Media type
	 * @param fieldName Field name, e.g. "identifications.taxonIds"
	 * @param value Single field value
	 * @param limit Maximum amount of results allowed
	 * @return All media that have matching metadata
	 * @throws ApiException An unexpected error happened while communicating with the API
	 */
	List<Media> search(MediaClass mediaClass, String fieldName, String value, int limit, int offset) throws ApiException;

	/**
	 * Retrieve all media matching query paramaters
	 *
	 * @param mediaClass Media type
	 * @param fieldName Field name, e.g. "identifications.taxonIds"
	 * @param values All allowed field values, ie. OR semantics
	 * @return All media that have matching metadata
	 * @throws ApiException An unexpected error happened while communicating with the API
	 */
	List<Media> search(MediaClass mediaClass, String fieldName, List<String> values) throws ApiException;

	/**
	 * Retrieve all media matching query parameters with specified limit and offset
	 *
	 * @param mediaClass Media type
	 * @param fieldName Field name, e.g. "identifications.taxonIds"
	 * @param values All allowed field values, ie. OR semantics
	 * @param limit Maximum amount of results allowed
	 * @return All media that have matching metadata
	 * @throws ApiException An unexpected error happened while communicating with the API
	 */
	List<Media> search(MediaClass mediaClass, String fieldName, List<String> values, int limit, int offset) throws ApiException;

	/**
	 * Returns all media that have identifications for the specified taxa
	 * and their respective subtaxa
	 *
	 * @param mediaClass Media type
	 * @param taxa 1..n taxon IDs
	 * @return A list of media
	 * @throws ApiException An unexpected error happened while communicating with the API
	 */
	List<Media> searchSubtaxonMedia(MediaClass mediaClass, String... taxa) throws ApiException;

	/**
	 * Returns all media that have identifications for the specified taxa
	 * and their respective subtaxa using specified limit and offset
	 *
	 * @param mediaClass Media type
	 * @param limit Maximum amount of results allowed
	 * @param taxa 1..n taxon IDs
	 * @return A list of media
	 * @throws ApiException An unexpected error happened while communicating with the API
	 */
	List<Media> searchSubtaxonMedia(MediaClass mediaClass, int limit, int offset, String... taxa) throws ApiException;

}
