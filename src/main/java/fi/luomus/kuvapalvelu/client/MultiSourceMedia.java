package fi.luomus.kuvapalvelu.client;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import fi.luomus.kuvapalvelu.model.MediaFileType;

public class MultiSourceMedia implements AutoCloseable, Iterable<MultiSourceMedia.MediaFileSource> {

	public class MediaFileSource {
		private final InputStream stream;
		private final String filename;
		private final MediaFileType type;

		private MediaFileSource(File file, MediaFileType type) {
			if (!file.exists()) {
				throw new IllegalArgumentException("file does not exist");
			}
			try {
				this.stream = new FileInputStream(file);
			} catch (FileNotFoundException e) {
				throw new RuntimeException(e);
			}
			this.filename = file.getName();
			this.type = type;
		}

		private MediaFileSource(InputStream stream, String filename, MediaFileType type) {
			this.stream = stream;
			this.filename = filename;
			this.type = type;
		}

		public InputStream getStream() {
			return stream;
		}
		public String getFilename() {
			return filename;
		}
		public MediaFileType getFileType() {
			return type;
		}
		private void close() {
			try {
				stream.close();
			} catch (Exception ex) {}
		}
	}

	private final List<MediaFileSource> files = new ArrayList<>();

	public MultiSourceMedia set(File file, MediaFileType type) {
		files.add(new MediaFileSource(file, type));
		return this;
	}

	public MultiSourceMedia set(InputStream stream, String filename, MediaFileType type) {
		files.add(new MediaFileSource(stream, filename, type));
		return this;
	}

	@Override
	public void close() {
		for (MediaFileSource e : files) {
			e.close();
		}
	}

	@Override
	public Iterator<MediaFileSource> iterator() {
		return files.iterator();
	}

}
