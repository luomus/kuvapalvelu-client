package fi.luomus.kuvapalvelu.client;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.ws.rs.ProcessingException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;
import org.glassfish.jersey.jackson.internal.jackson.jaxrs.json.JacksonJsonProvider;
import org.glassfish.jersey.media.multipart.FormDataMultiPart;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.media.multipart.file.StreamDataBodyPart;

import fi.luomus.kuvapalvelu.model.FileUploadResponse;
import fi.luomus.kuvapalvelu.model.License;
import fi.luomus.kuvapalvelu.model.LifeStage;
import fi.luomus.kuvapalvelu.model.Media;
import fi.luomus.kuvapalvelu.model.MediaClass;
import fi.luomus.kuvapalvelu.model.MediaFileType;
import fi.luomus.kuvapalvelu.model.Meta;
import fi.luomus.kuvapalvelu.model.NamedResource;
import fi.luomus.kuvapalvelu.model.NewMedia;
import fi.luomus.kuvapalvelu.model.NewMultiSourceMedia;
import fi.luomus.kuvapalvelu.model.NewMultiSourceMedia.MediaFile;
import fi.luomus.kuvapalvelu.model.Sex;
import fi.luomus.kuvapalvelu.model.Side;
import fi.luomus.kuvapalvelu.model.Type;
import fi.luomus.utils.exceptions.ApiException;
import fi.luomus.utils.exceptions.NotFoundException;

public class MediaApiClientImpl implements MediaApiClient {

	private static final String TAXON_FIELD = "identifications.taxonIds";

	private final MediaApiPreferences preferences;
	private WebTarget target;
	private Client client;

	public MediaApiClientImpl(MediaApiPreferences preferences) {
		this.preferences = preferences;
	}

	@Override
	public String uploadImage(File file, Meta meta) throws ApiException {
		return upload(MediaClass.IMAGE, file, meta);
	}

	@Override
	public String uploadAudio(File file, Meta meta) throws ApiException {
		return upload(MediaClass.AUDIO, file, meta);
	}

	@Override
	public String uploadPdf(File file, Meta meta) throws ApiException {
		return upload(MediaClass.PDF, file, meta);
	}

	private String upload(MediaClass mediaClass, File file, Meta meta) throws ApiException {
		if (!file.exists()) {
			throw new IllegalArgumentException("file does not exist");
		}
		try (InputStream stream = new FileInputStream(file)) {
			return upload(mediaClass, stream, file.getName(), meta);
		} catch (FileNotFoundException e) {
			throw new IllegalArgumentException("file does not exist");
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public String uploadImage(InputStream stream, String filename, Meta meta) throws ApiException {
		return upload(MediaClass.IMAGE, stream, filename, meta);
	}

	@Override
	public String uploadAudio(InputStream stream, String filename, Meta meta) throws ApiException {
		return upload(MediaClass.AUDIO, stream, filename, meta);
	}

	@Override
	public String uploadPdf(InputStream stream, String filename, Meta meta) throws ApiException {
		return upload(MediaClass.PDF, stream, filename, meta);
	}

	private String upload(MediaClass mediaClass, InputStream stream, String filename, Meta meta) throws ApiException {
		FileUploadResponse uploadResponse = uploadFile(mediaClass, stream, filename);
		NewMedia newMedia = new NewMedia()
				.setTempFileId(uploadResponse.getId())
				.setMeta(meta.latestVersion())
				.setMediaClass(mediaClass);
		String id = createMedia(newMedia);
		return id;
	}

	@Override
	public String upload(MediaClass mediaClass, MultiSourceMedia multiFileMedia, Meta meta) throws ApiException {
		NewMultiSourceMedia newMedia = new NewMultiSourceMedia()
				.setMediaClass(mediaClass)
				.setMeta(meta);

		for (MultiSourceMedia.MediaFileSource file : multiFileMedia) {
			FileUploadResponse response = uploadFile(mediaClass, file.getFileType(), file.getStream(), file.getFilename());
			newMedia.getEntries().add(new MediaFile(response.getId(), file.getFileType(), file.getFilename()));
		}

		String id = createMedia(newMedia);
		return id;
	}

	private FileUploadResponse uploadFile(MediaClass mediaClass, InputStream stream, String filename) throws ApiException {
		return uploadFile(mediaClass, null, stream, filename);
	}

	private FileUploadResponse uploadFile(MediaClass mediaClass, MediaFileType fileType, InputStream stream, String filename) throws ApiException {
		FormDataMultiPart multiPart = null;
		Response response = null;
		try {
			multiPart = new FormDataMultiPart();
			String bodyPartName = fileType != null ? fileType.name() : mediaClass.name();
			multiPart.bodyPart(new StreamDataBodyPart(bodyPartName, stream, filename));

			response = target()
					.path("api/fileUpload")
					.queryParam("mediaClass", mediaClass.name()).request()
					.post(Entity.entity(multiPart, multiPart.getMediaType()));

			if (response.getStatus() == Response.Status.BAD_REQUEST.getStatusCode()) {
				try {
					Map<String, String> error = response.readEntity(new GenericType<Map<String, String>>() {});
					String errorCode = error.get("code");
					if ("INVALID_FILE".equals(errorCode)) {
						throw new MediaFormatNotSupported("Unable to process given media file: " + error);
					}
				} catch (ProcessingException e) {
					// nop
				}
			}

			raiseForNonSuccess(response);

			return response.readEntity(new GenericType<List<FileUploadResponse>>() {}).get(0);
		} finally {
			if (multiPart != null) {
				try {
					multiPart.close();
				} catch (IOException e) {
					throw new RuntimeException(e);
				}
			}
			if (response != null) {
				response.close();
			}
		}
	}

	private String createMedia(NewMedia newMedia) throws ApiException {
		Response response = null;
		try {
			response = target().path("api/" + path(newMedia.getMediaClass()))
					.request(MediaType.APPLICATION_JSON_TYPE)
					.post(Entity.json(Arrays.asList(newMedia)));
			raiseForNonSuccess(response);
			return response.readEntity(new GenericType<List<Media>>() {}).get(0).getId();
		} finally {
			if (response != null) {
				response.close();
			}
		}
	}

	private String createMedia(NewMultiSourceMedia newMedia) throws ApiException {
		Response response = null;
		try {
			response = target().path("api/" + path(newMedia.getMediaClass()) + "/multi")
					.request(MediaType.APPLICATION_JSON_TYPE)
					.post(Entity.json(Arrays.asList(newMedia)));
			raiseForNonSuccess(response);
			return response.readEntity(new GenericType<List<Media>>() {}).get(0).getId();
		} finally {
			if (response != null) {
				response.close();
			}
		}
	}

	private String path(MediaClass mediaClass) {
		if (mediaClass == null) throw new IllegalArgumentException("Null media class");
		return mediaClass.getApiPath();
	}

	@Override
	public void update(MediaClass mediaClass, String id, Meta newMeta) throws ApiException, NotFoundException {
		Response response = null;
		try {
			response = target().path("api")
					.path(path(mediaClass))
					.path(id)
					.request()
					.put(Entity.json(newMeta.latestVersion()));

			if (isNotFound(response)) {
				throw new NotFoundException();
			}
			raiseForNonSuccess(response);
		} finally {
			if (response != null) {
				response.close();
			}
		}

	}

	@Override
	public void delete(MediaClass mediaClass, String id) throws ApiException, NotFoundException {
		Response response = null;
		try {
			response = target().path("api")
					.path(path(mediaClass))
					.path(id)
					.request()
					.delete();
			if (isNotFound(response)) {
				throw new NotFoundException();
			}
			raiseForNonSuccess(response);
		} finally {
			if (response != null) {
				response.close();
			}
		}
	}

	@Override
	public Optional<Media> get(MediaClass mediaClass, String id) throws ApiException {
		Response response = null;
		try {
			response = target().path("api")
					.path(path(mediaClass))
					.path(id)
					.request()
					.get();
			if (isNotFound(response)) {
				return Optional.empty();
			}
			raiseForNonSuccess(response);

			Media media = response.readEntity(Media.class);
			media.getMeta().latestVersion();
			return Optional.of(media);
		} finally {
			if (response != null) {
				response.close();
			}
		}
	}

	@Override
	public List<Media> search(MediaClass mediaClass, String fieldName, String value) throws ApiException {
		return this.search(mediaClass, fieldName, Arrays.asList(value));
	}

	@Override
	public List<Media> search(MediaClass mediaClass, String fieldName, List<String> values) throws ApiException {
		return this.searchCombining(mediaClass, fieldName, values, false);
	}

	@Override
	public List<Media> searchSubtaxonMedia(MediaClass mediaClass, String... taxa) throws ApiException {
		return this.searchCombining(mediaClass, TAXON_FIELD, Arrays.asList(taxa), true);
	}

	@Override
	public List<Media> search(MediaClass mediaClass, String fieldName, String value, int limit, int offset) throws ApiException {
		return this.search(mediaClass, fieldName, Arrays.asList(value), limit, offset);
	}

	@Override
	public List<Media> search(MediaClass mediaClass, String fieldName, List<String> values, int limit, int offset) throws ApiException {
		return this.searchActual(mediaClass, fieldName, values, false, limit, offset);
	}

	@Override
	public List<Media> searchSubtaxonMedia(MediaClass mediaClass, int limit, int offset, String... taxa) throws ApiException {
		return this.searchActual(mediaClass, TAXON_FIELD, Arrays.asList(taxa), true, limit, offset);
	}

	private List<Media> searchCombining(MediaClass mediaClass, String fieldName, List<String> values, boolean searchForSubtaxons) throws ApiException {
		int currentOffset = 0;
		final int limit = 2000;

		List<Media> result = new ArrayList<>();
		int lastAmount;
		do {
			List<Media> page = searchActual(mediaClass, fieldName, values, searchForSubtaxons, limit, currentOffset);
			lastAmount = page.size();
			result.addAll(page);
		} while (lastAmount >= limit);

		return result;
	}

	private List<Media> searchActual(MediaClass mediaClass, String fieldName, List<String> values, boolean searchForSubtaxons, Integer limit, Integer offset) throws ApiException {
		if (limit != null && limit <= 0) {
			throw new IllegalArgumentException("if present, limit must be >0");
		}
		if (offset != null && offset < 0) {
			throw new IllegalArgumentException("if present, offset must be >=0");
		}
		if (fieldName == null) {
			throw new IllegalArgumentException("Field name must be provided");
		}
		if (values == null || values.size() == 0) {
			throw new IllegalArgumentException("At least one value must be provided");
		}
		if (searchForSubtaxons && !TAXON_FIELD.equals(fieldName)) {
			throw new IllegalArgumentException("Can't search for sub-taxons for non-taxon field");
		}

		Response response = null;
		try {
			WebTarget webTarget = target().path("api").path(path(mediaClass))
					.queryParam(fieldName);
			if (limit != null) {
				webTarget = webTarget.queryParam("limit", limit);
			}
			if (offset != null) {
				webTarget = webTarget.queryParam("offset", offset);
			}
			for (String value : values) {
				webTarget = webTarget.queryParam(fieldName, value);
			}
			webTarget = webTarget.queryParam("searchForSubTaxons", searchForSubtaxons);
			response = webTarget.request(MediaType.APPLICATION_JSON_TYPE).get();
			raiseForNonSuccess(response);

			return response.readEntity(new GenericType<List<Media>>() {});
		} finally {
			if (response != null) {
				response.close();
			}
		}
	}

	@Override
	public List<License> getLicenses() throws ApiException {
		return getNamedResource("licenses");
	}

	@Override
	public List<Sex> getSexes() throws ApiException {
		return getNamedResource("sexes");
	}

	@Override
	public List<LifeStage> getLifeStages() throws ApiException {
		return getNamedResource("lifeStages");
	}

	@Override
	public List<LifeStage> getPlantLifeStages() throws ApiException {
		return getNamedResource("plantLifeStages");
	}

	@Override
	public List<Side> getSides() throws ApiException {
		return getNamedResource("sides");
	}

	@Override
	public List<Type> getTypes() throws ApiException {
		return getNamedResource("types");
	}

	private <T extends NamedResource> List<T> getNamedResource(String resourceName) throws ApiException {
		Response response = null;
		try {
			response = target().path("api/"+resourceName).request().get();
			raiseForNonSuccess(response);
			return response.readEntity(new GenericType<List<T>>() {});
		} finally {
			if (response != null) {
				response.close();
			}
		}
	}

	private WebTarget target() {
		if (this.target != null) return target;

		// Prevent duplicate target creations
		synchronized (this) {
			if (this.target == null) {
				if (client == null) {
					client = ClientBuilder.newBuilder()
							.register(MultiPartFeature.class)
							.register(JacksonJsonProvider.class)
							.build();
				}
				this.target = client.target(preferences.getBaseUri())
						.register(HttpAuthenticationFeature.basic(preferences.getUsername(), preferences.getPassword()));
			}
		}

		return this.target;
	}

	private void raiseForNonSuccess(Response response) throws ApiException {
		if (!response.getStatusInfo().getFamily().equals(Response.Status.Family.SUCCESSFUL)) {
			String cause = null;
			try {
				cause = response.readEntity(String.class);
			} catch (ProcessingException e) {
				// nop
			}
			throw ApiException.builder().source(MediaApiClient.CLIENT_IDENTIFIER)
			.code(Integer.toString(response.getStatus()))
			.details(cause)
			.build();
		}
	}

	private boolean isNotFound(Response response) {
		return response.getStatusInfo().getStatusCode() == Response.Status.NOT_FOUND.getStatusCode();
	}

	public static class MediaApiClientImplBuilder {

		private final URI baseUri;
		private final String username;
		private final String password;

		private MediaApiClientImplBuilder(URI baseUri, String username, String password) {
			this.baseUri = baseUri;
			this.username = username;
			this.password = password;
		}

		public MediaApiClientImplBuilder uri(String uriString) {
			try {
				URI uri = new URI(uriString);

				return uri(uri);
			} catch (URISyntaxException e) {
				throw new IllegalArgumentException("invalid URI format", e);
			}
		}

		public MediaApiClientImplBuilder uri(URI uri) {
			return new MediaApiClientImplBuilder(uri, username, password);
		}

		public MediaApiClientImplBuilder username(String username) {
			return new MediaApiClientImplBuilder(baseUri, username, password);
		}

		public MediaApiClientImplBuilder password(String password) {
			return new MediaApiClientImplBuilder(baseUri, username, password);
		}

		public MediaApiClientImpl build() {
			if (baseUri == null || username == null || password == null) {
				throw new IllegalStateException("missing argument");
			}
			return new MediaApiClientImpl(new MediaApiPreferences() {
				@Override
				public URI getBaseUri() {
					return baseUri;
				}

				@Override
				public String getUsername() {
					return username;
				}

				@Override
				public String getPassword() {
					return password;
				}
			});
		}
	}

	public static MediaApiClientImplBuilder builder() {
		return new MediaApiClientImplBuilder(null, null, null);
	}

	@Override
	public void close() {
		if (client != null) client.close();
	}

}
