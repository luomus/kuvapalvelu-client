package fi.luomus.kuvapalvelu.client;

public class MediaFormatNotSupported extends RuntimeException {

	private static final long serialVersionUID = 1843034787474539330L;

	public MediaFormatNotSupported() {}

	public MediaFormatNotSupported(String message) {
		super(message);
	}

}