package fi.luomus.kuvapalvelu.model;

import org.joda.time.DateTime;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import fi.luomus.utils.serialization.DateTimeToEpochSecondsSerializer;
import fi.luomus.utils.serialization.EpochSecondsToDateTimeDeserializer;

public class FileUploadResponse {

	private String name;
	private String filename;
	private String id;

	@JsonDeserialize(using = EpochSecondsToDateTimeDeserializer.class)
	@JsonSerialize(using = DateTimeToEpochSecondsSerializer.class)
	private DateTime expires;

	/**
	 * @return When uploading media, it is possible to submit multiple media at the same time as request body parts. This is name given to the body part.
	 */
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return Original filename (not cleaned by the API)
	 */
	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	/**
	 * @return Temporary id provided for the media file. To be used when submitting metadata for the media file(s).
	 */
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return Expiry time for the temporary id / media file.
	 */
	public DateTime getExpires() {
		return expires;
	}

	public void setExpires(DateTime expires) {
		this.expires = expires;
	}

	@Override
	public String toString() {
		return "FileUploadResponse [name=" + name + ", filename=" + filename + ", id=" + id + ", expires=" + expires + "]";
	}

}
