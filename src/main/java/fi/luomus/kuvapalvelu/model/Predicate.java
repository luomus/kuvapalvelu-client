package fi.luomus.kuvapalvelu.model;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface Predicate {

	public final static String DEFAULT_NS = "http://tun.fi/";

	String namespace() default DEFAULT_NS;

	String value();

	/**
	 * True if the value is a String literal, such as the person name.
	 * False if the value is reference to a resource, such as taxon or person identifier
	 */
	boolean isLiteral() default true;

	boolean isNullable() default true;

}
