package fi.luomus.kuvapalvelu.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.joda.time.DateTime;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import fi.luomus.utils.model.LocalizedString;
import fi.luomus.utils.serialization.DateTimeToEpochSecondsSerializer;
import fi.luomus.utils.serialization.EpochSecondsToDateTimeDeserializer;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Meta implements Comparable<Meta> {

	public static final String LATEST_META_VERSION = "v2";

	public Meta latestVersion() {
		if (metadataVersion == null) {
			setMetadataVersion(LATEST_META_VERSION);
		}
		return this;
	}

	public String metadataVersion;

	@Predicate(value = "MM.sourceSystem", isNullable = false, isLiteral = false)
	public String sourceSystem;

	@Predicate("MM.uploadDateTime")
	@JsonSerialize(using = DateTimeToEpochSecondsSerializer.class)
	@JsonDeserialize(using = EpochSecondsToDateTimeDeserializer.class)
	public DateTime uploadDateTime;

	@Predicate("MM.modifiedDateTime")
	@JsonSerialize(using = DateTimeToEpochSecondsSerializer.class)
	@JsonDeserialize(using = EpochSecondsToDateTimeDeserializer.class)
	public DateTime modifiedDateTime;

	@Predicate(value = "MZ.intellectualRights", isNullable=false, isLiteral = false)
	public String license;

	@Predicate(value="MZ.intellectualOwner", isNullable=false)
	public String rightsOwner;

	public boolean secret = false;

	@Predicate("MM.capturerVerbatim")
	public List<String> capturers = new ArrayList<>();

	@Predicate("MM.captureDateTime")
	@JsonSerialize(using = DateTimeToEpochSecondsSerializer.class)
	@JsonDeserialize(using = EpochSecondsToDateTimeDeserializer.class)
	public DateTime captureDateTime;

	@Predicate(value="MM.uploadedBy")
	public String uploadedBy;

	@Predicate(value="MM.modifiedBy")
	public String modifiedBy;

	@Predicate("MM.originalFilename")
	public String originalFilename;

	@Predicate("MM.documentURI")
	public List<String> documentIds = new ArrayList<>();

	@Predicate("MM.keyword")
	public List<String> tags = new ArrayList<>();

	public Identifications identifications = new Identifications();

	@Predicate("MM.caption")
	public String caption;

	@Predicate("MM.taxonDescriptionCaption")
	public LocalizedString taxonDescriptionCaption = new LocalizedString();

	@Predicate(value = "MY.sex", isLiteral = false)
	public List<String> sex = new ArrayList<>();

	@Predicate(value = "MY.lifeStage", isLiteral = false)
	public List<String> lifeStage = new ArrayList<>();

	@Predicate(value = "MY.plantLifeStage", isLiteral = false)
	public List<String> plantLifeStage = new ArrayList<>();

	@Predicate(value = "MM.primaryForTaxon", isLiteral = false)
	public List<String> primaryForTaxon = new ArrayList<>();

	@Predicate(value = "MM.side", isLiteral = false)
	public String side;

	@Predicate(value = "MM.type", isLiteral = false)
	public String type;

	@Predicate("sortOrder")
	public Integer sortOrder;

	@Predicate("MM.fullResolutionMediaAvailable")
	public Boolean fullResolutionMediaAvailable;

	@JsonIgnoreProperties(ignoreUnknown = true)
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	public static class Identifications {
		@Predicate(value = "MM.taxonURI", isLiteral = false)
		public List<String> taxonIds = new ArrayList<>();
		@Predicate("MM.taxonVerbatim")
		public List<String> verbatim = new ArrayList<>();

		/**
		 * @return taxon qnames (e.g. MX.123) to which this media is associated with
		 */
		public List<String> getTaxonIds() {
			if (taxonIds == null) setTaxonIds(new ArrayList<>());
			return taxonIds;
		}

		public Identifications setTaxonIds(List<String> taxonIds) {
			this.taxonIds = taxonIds;
			return this;
		}

		public Identifications addTaxonId(String taxonId) {
			getTaxonIds().add(taxonId);
			return this;
		}

		public Identifications removeTaxonId(String taxonId) {
			getTaxonIds().remove(taxonId);
			return this;
		}

		/**
		 * @return Taxon verbatim (taxon name, finnish, latin, or other)
		 */
		public List<String> getVerbatim() {
			if (verbatim == null) setVerbatim(new ArrayList<>());
			return verbatim;
		}

		public Identifications setVerbatim(List<String> verbatim) {
			this.verbatim = verbatim;
			return this;
		}

		public Identifications addVerbatim(String verbatim) {
			getVerbatim().add(verbatim);
			return this;
		}

		@Override
		public boolean equals(Object o) {
			if (this == o) return true;
			if (o == null || getClass() != o.getClass()) return false;

			Identifications that = (Identifications) o;

			if (taxonIds != null ? !taxonIds.equals(that.taxonIds) : that.taxonIds != null) return false;
			if (verbatim != null ? !verbatim.equals(that.verbatim) : that.verbatim != null) return false;

			return true;
		}

		@Override
		public int hashCode() {
			int result = taxonIds != null ? taxonIds.hashCode() : 0;
			result = 31 * result + (verbatim != null ? verbatim.hashCode() : 0);
			return result;
		}

		@Override
		public String toString() {
			return "Identifications [taxonIds=" + taxonIds + ", verbatim=" + verbatim + "]";
		}

	}

	/**
	 * @return Is an authorization token required to view the media?
	 */
	public boolean isSecret() {
		return secret;
	}

	public Meta setSecret(boolean secret) {
		this.secret = secret;
		return this;
	}

	/**
	 * @return source system qname that uploaded the media (e.g. KE.123)
	 */
	public String getSourceSystem() {
		return sourceSystem;
	}

	public Meta setSourceSystem(String sourceSystem) {
		this.sourceSystem = sourceSystem;
		return this;
	}

	/**
	 * @return The client system-specific actor (usually an username) that uploaded the media
	 */
	public String getUploadedBy() {
		return uploadedBy;
	}

	public Meta setUploadedBy(String uploadedBy) {
		this.uploadedBy = uploadedBy;
		return this;
	}
	/**
	 * @return The client system-specific actor (usually an username) that last modified media metadata
	 */
	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	/**
	 * @return System-specific URI for the media (e.g. a link to the form submission that
	 *         contained the original media)
	 */
	public List<String> getDocumentIds() {
		if (documentIds == null) setDocumentIds(new ArrayList<>());
		return documentIds;
	}

	public Meta setDocumentIds(List<String> documentIds) {
		this.documentIds = documentIds;
		return this;
	}

	public Meta addDocumentId(String id) {
		getDocumentIds().add(id);
		return this;
	}

	public Boolean getSecret() {
		return secret;
	}

	/**
	 * @return List of system-specific free-form tags
	 */
	public List<String> getTags() {
		if (tags == null) setTags(new ArrayList<>());
		return tags;
	}

	public Meta setTags(List<String> tags) {
		this.tags = tags;
		return this;
	}

	public Meta addTag(String tag) {
		getTags().add(tag);
		return this;
	}

	/**
	 * @return When was the media in question captured (e.g. when the photograph was taken)
	 */
	public DateTime getCaptureDateTime() {
		return captureDateTime;
	}

	public Meta setCaptureDateTime(DateTime captureDateTime) {
		this.captureDateTime = captureDateTime;
		return this;
	}

	/**
	 * @return Get the identifications for the species found in the media
	 */
	public Identifications getIdentifications() {
		if (identifications == null) setIdentifications(new Identifications());
		return identifications;
	}

	public Meta setIdentifications(Identifications identifications) {
		if (identifications == null) identifications = new Identifications();
		this.identifications = identifications;
		return this;
	}

	public Meta addTaxonId(String taxonId) {
		getIdentifications().addTaxonId(taxonId);
		return this;
	}

	public Meta addTaxonVerbatim(String verbatim) {
		getIdentifications().addVerbatim(verbatim);
		return this;
	}

	/**
	 * @return URI that can be used to determine the license of the media unambigiously
	 */
	public String getLicense() {
		return license;
	}

	public Meta setLicense(String license) {
		this.license = license;
		return this;
	}

	/**
	 * @return Free-form string that tells who owns the original rights to the media
	 */
	public String getRightsOwner() {
		return rightsOwner;
	}

	public Meta setRightsOwner(String rightsOwner) {
		this.rightsOwner = rightsOwner;
		return this;
	}

	/**
	 * @return Name of the actors that originally created the media
	 */
	public List<String> getCapturers() {
		if (capturers == null) setCapturers(new ArrayList<>());
		return capturers;
	}

	public Meta setCapturers(List<String> capturers) {
		this.capturers = capturers;
		return this;
	}

	public Meta addCapturer(String capturer) {
		getCapturers().add(capturer);
		return this;
	}

	/**
	 * @return The original filename of the uploaded media
	 */
	public String getOriginalFilename() {
		return originalFilename;
	}

	public Meta setOriginalFilename(String originalFilename) {
		this.originalFilename = originalFilename;
		return this;
	}

	/**
	 * @return When the media resource was created
	 */
	public DateTime getUploadDateTime() {
		return uploadDateTime;
	}

	public Meta setUploadDateTime(DateTime uploadDateTime) {
		this.uploadDateTime = uploadDateTime;
		return this;
	}

	/**
	 * @return When the media resource metadata was last modified
	 */
	public DateTime getModifiedDateTime() {
		return modifiedDateTime;
	}

	public void setModifiedDateTime(DateTime modifiedDateTime) {
		this.modifiedDateTime = modifiedDateTime;
	}

	/**
	 * @return Sorting order of the media (within taxon - note that in theory a media item can belong to multiple taxon, thus this is sort order across all taxa)
	 */
	public Integer getSortOrder() {
		return sortOrder;
	}

	public Meta setSortOrder(Integer sortOrder) {
		this.sortOrder = sortOrder;
		return this;
	}

	public String getCaption() {
		return caption;
	}

	public Meta setCaption(String caption) {
		this.caption = caption;
		return this;
	}

	public LocalizedString getTaxonDescriptionCaption() {
		if (taxonDescriptionCaption == null) setTaxonDescriptionCaption(new LocalizedString());
		return taxonDescriptionCaption;
	}

	public Meta setTaxonDescriptionCaption(LocalizedString taxonDescriptionCaption) {
		if (taxonDescriptionCaption == null) taxonDescriptionCaption = new LocalizedString();
		this.taxonDescriptionCaption = taxonDescriptionCaption;
		return this;
	}

	public Meta addTaxonDescriptionCaption(String caption, String locale) {
		getTaxonDescriptionCaption().put(locale, caption);
		return this;
	}

	/**
	 * @return Sexes in this media - MY.sexes values
	 */
	public List<String> getSex() {
		if (sex == null) setSex(new ArrayList<>());
		return sex;
	}

	public Meta setSex(List<String> sexes) {
		this.sex = sexes;
		return this;
	}

	public Meta addSex(String sex) {
		getSex().add(sex);
		return this;
	}

	/**
	 * @return Life stages in this media (non-plants) - MY.lifeStages values
	 */
	public List<String> getLifeStage() {
		if (lifeStage == null) setLifeStage(new ArrayList<>());
		return lifeStage;
	}

	public Meta setLifeStage(List<String> lifeStages) {
		this.lifeStage = lifeStages;
		return this;
	}

	public Meta addLifeStage(String lifeStage) {
		getLifeStage().add(lifeStage);
		return this;
	}

	/**
	 * @return Life stages in this media (plants) - MY.plantLifeStageEnum values
	 */
	public List<String> getPlantLifeStage() {
		if (plantLifeStage == null) setPlantLifeStage(new ArrayList<>());
		return plantLifeStage;
	}

	public Meta setPlantLifeStage(List<String> lifeStages) {
		this.plantLifeStage = lifeStages;
		return this;
	}

	public Meta addPlantLifeStage(String lifeStage) {
		getPlantLifeStage().add(lifeStage);
		return this;
	}

	/**
	 * @return taxon qnames (e.g. MX.123) for which this media is a primary image/media
	 */
	public List<String> getPrimaryForTaxon() {
		if (primaryForTaxon == null) setPrimaryForTaxon(new ArrayList<>());
		return primaryForTaxon;
	}

	public Meta setPrimaryForTaxon(List<String> taxonIds) {
		this.primaryForTaxon = taxonIds;
		return this;
	}

	public Meta addPrimaryForTaxon(String taxonId) {
		if (!getPrimaryForTaxon().contains(taxonId)) {
			getPrimaryForTaxon().add(taxonId);
		}
		return this;
	}

	public Meta removePrimaryForTaxon(String taxonId) {
		getPrimaryForTaxon().remove(taxonId);
		return this;
	}

	public boolean isPrimaryForTaxon(String taxonId) {
		return getPrimaryForTaxon().contains(taxonId);
	}

	/**
	 * @return Type of the media - MM.typeEnum values
	 */
	public String getType() {
		return type;
	}

	public Meta setType(String type) {
		this.type = type;
		return this;
	}

	/**
	 * @return Side of the media (upside, downside) - MM.sideEnum values
	 */
	public String getSide() {
		return side;
	}

	public Meta setSide(String side) {
		this.side = side;
		return this;
	}

	public Boolean getFullResolutionMediaAvailable() {
		return fullResolutionMediaAvailable;
	}

	public Meta setFullResolutionMediaAvailable(Boolean fullResolutionMediaAvailable) {
		this.fullResolutionMediaAvailable = fullResolutionMediaAvailable;
		return this;
	}

	public String getMetadataVersion() {
		return metadataVersion;
	}

	public Meta setMetadataVersion(String metadataVersion) {
		this.metadataVersion = metadataVersion;
		return this;
	}

	@Override
	public int compareTo(Meta o) {
		if (this.getSortOrder() != null && o.getSortOrder() != null) {
			return this.getSortOrder().compareTo(o.getSortOrder());
		} else if (this.getSortOrder() != null) {
			return -1;
		} else if (o.getSortOrder() != null) {
			return 1;
		} else {
			return 0;
		}
	}

	@Override
	public String toString() {
		return "Meta [metadataVersion=" + metadataVersion + ", sourceSystem=" + sourceSystem + ", uploadDateTime=" + uploadDateTime + ", modifiedDateTime=" + modifiedDateTime
				+ ", license=" + license + ", rightsOwner=" + rightsOwner + ", secret=" + secret + ", capturers=" + capturers + ", captureDateTime=" + captureDateTime
				+ ", uploadedBy=" + uploadedBy + ", modifiedBy=" + modifiedBy + ", originalFilename=" + originalFilename + ", documentIds=" + documentIds + ", tags=" + tags
				+ ", identifications=" + identifications + ", caption=" + caption + ", taxonDescriptionCaption=" + taxonDescriptionCaption + ", sex=" + sex + ", lifeStage="
				+ lifeStage + ", plantLifeStage=" + plantLifeStage + ", primaryForTaxon=" + primaryForTaxon + ", side=" + side + ", type=" + type + ", sortOrder=" + sortOrder
				+ ", fullResolutionMediaAvailable=" + fullResolutionMediaAvailable + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((caption == null) ? 0 : caption.hashCode());
		result = prime * result + ((captureDateTime == null) ? 0 : captureDateTime.hashCode());
		result = prime * result + ((capturers == null) ? 0 : capturers.hashCode());
		result = prime * result + ((documentIds == null) ? 0 : documentIds.hashCode());
		result = prime * result + ((fullResolutionMediaAvailable == null) ? 0 : fullResolutionMediaAvailable.hashCode());
		result = prime * result + ((identifications == null) ? 0 : identifications.hashCode());
		result = prime * result + ((license == null) ? 0 : license.hashCode());
		result = prime * result + ((lifeStage == null) ? 0 : lifeStage.hashCode());
		result = prime * result + ((metadataVersion == null) ? 0 : metadataVersion.hashCode());
		result = prime * result + ((modifiedBy == null) ? 0 : modifiedBy.hashCode());
		result = prime * result + ((modifiedDateTime == null) ? 0 : modifiedDateTime.hashCode());
		result = prime * result + ((originalFilename == null) ? 0 : originalFilename.hashCode());
		result = prime * result + ((plantLifeStage == null) ? 0 : plantLifeStage.hashCode());
		result = prime * result + ((primaryForTaxon == null) ? 0 : primaryForTaxon.hashCode());
		result = prime * result + ((rightsOwner == null) ? 0 : rightsOwner.hashCode());
		result = prime * result + (secret ? 1231 : 1237);
		result = prime * result + ((sex == null) ? 0 : sex.hashCode());
		result = prime * result + ((side == null) ? 0 : side.hashCode());
		result = prime * result + ((sortOrder == null) ? 0 : sortOrder.hashCode());
		result = prime * result + ((sourceSystem == null) ? 0 : sourceSystem.hashCode());
		result = prime * result + ((tags == null) ? 0 : tags.hashCode());
		result = prime * result + ((taxonDescriptionCaption == null) ? 0 : taxonDescriptionCaption.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		result = prime * result + ((uploadDateTime == null) ? 0 : uploadDateTime.hashCode());
		result = prime * result + ((uploadedBy == null) ? 0 : uploadedBy.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Meta other = (Meta) obj;
		if (caption == null) {
			if (other.caption != null)
				return false;
		} else if (!caption.equals(other.caption))
			return false;
		if (captureDateTime == null) {
			if (other.captureDateTime != null)
				return false;
		} else if (!captureDateTime.equals(other.captureDateTime))
			return false;
		if (capturers == null) {
			if (other.capturers != null)
				return false;
		} else if (!capturers.equals(other.capturers))
			return false;
		if (documentIds == null) {
			if (other.documentIds != null)
				return false;
		} else if (!documentIds.equals(other.documentIds))
			return false;
		if (fullResolutionMediaAvailable == null) {
			if (other.fullResolutionMediaAvailable != null)
				return false;
		} else if (!fullResolutionMediaAvailable.equals(other.fullResolutionMediaAvailable))
			return false;
		if (identifications == null) {
			if (other.identifications != null)
				return false;
		} else if (!identifications.equals(other.identifications))
			return false;
		if (license == null) {
			if (other.license != null)
				return false;
		} else if (!license.equals(other.license))
			return false;
		if (lifeStage == null) {
			if (other.lifeStage != null)
				return false;
		} else if (!lifeStage.equals(other.lifeStage))
			return false;
		if (metadataVersion == null) {
			if (other.metadataVersion != null)
				return false;
		} else if (!metadataVersion.equals(other.metadataVersion))
			return false;
		if (modifiedBy == null) {
			if (other.modifiedBy != null)
				return false;
		} else if (!modifiedBy.equals(other.modifiedBy))
			return false;
		if (modifiedDateTime == null) {
			if (other.modifiedDateTime != null)
				return false;
		} else if (!modifiedDateTime.equals(other.modifiedDateTime))
			return false;
		if (originalFilename == null) {
			if (other.originalFilename != null)
				return false;
		} else if (!originalFilename.equals(other.originalFilename))
			return false;
		if (plantLifeStage == null) {
			if (other.plantLifeStage != null)
				return false;
		} else if (!plantLifeStage.equals(other.plantLifeStage))
			return false;
		if (primaryForTaxon == null) {
			if (other.primaryForTaxon != null)
				return false;
		} else if (!primaryForTaxon.equals(other.primaryForTaxon))
			return false;
		if (rightsOwner == null) {
			if (other.rightsOwner != null)
				return false;
		} else if (!rightsOwner.equals(other.rightsOwner))
			return false;
		if (secret != other.secret)
			return false;
		if (sex == null) {
			if (other.sex != null)
				return false;
		} else if (!sex.equals(other.sex))
			return false;
		if (side == null) {
			if (other.side != null)
				return false;
		} else if (!side.equals(other.side))
			return false;
		if (sortOrder == null) {
			if (other.sortOrder != null)
				return false;
		} else if (!sortOrder.equals(other.sortOrder))
			return false;
		if (sourceSystem == null) {
			if (other.sourceSystem != null)
				return false;
		} else if (!sourceSystem.equals(other.sourceSystem))
			return false;
		if (tags == null) {
			if (other.tags != null)
				return false;
		} else if (!tags.equals(other.tags))
			return false;
		if (taxonDescriptionCaption == null) {
			if (other.taxonDescriptionCaption != null)
				return false;
		} else if (!taxonDescriptionCaption.equals(other.taxonDescriptionCaption))
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		if (uploadDateTime == null) {
			if (other.uploadDateTime != null)
				return false;
		} else if (!uploadDateTime.equals(other.uploadDateTime))
			return false;
		if (uploadedBy == null) {
			if (other.uploadedBy != null)
				return false;
		} else if (!uploadedBy.equals(other.uploadedBy))
			return false;
		return true;
	}



	private interface LegacyConversion {
		void convert(String tag, Meta meta);
	}

	private static LegacyConversion LEGACY_SPECIMEN = new LegacyConversion() {
		@Override
		public void convert(String tag, Meta meta) {
			if (notGiven(meta.getType())) meta.setType("MM.typeEnumSpecimen");
		}
	};

	private static LegacyConversion LEGACY_LABEL = new LegacyConversion() {
		@Override
		public void convert(String tag, Meta meta) {
			if (notGiven(meta.getType())) meta.setType("MM.typeEnumLabel");
		}
	};

	private static LegacyConversion LEGACY_LIVE = new LegacyConversion() {
		@Override
		public void convert(String tag, Meta meta) {
			if (notGiven(meta.getType())) meta.setType("MM.typeEnumLive");
		}
	};

	private static LegacyConversion LEGACY_HABITAT = new LegacyConversion() {
		@Override
		public void convert(String tag, Meta meta) {
			if (notGiven(meta.getType())) meta.setType("MM.typeEnumHabitat");
		}
	};

	private static LegacyConversion LEGACY_CARCASS = new LegacyConversion() {
		@Override
		public void convert(String tag, Meta meta) {
			if (notGiven(meta.getType())) meta.setType("MM.typeEnumCarcass");
		}
	};

	private static LegacyConversion LEGACY_MICROSCOPY = new LegacyConversion() {
		@Override
		public void convert(String tag, Meta meta) {
			if (notGiven(meta.getType())) meta.setType("MM.typeEnumMicroscopy");
		}
	};

	private static LegacyConversion LEGACY_SKELETAL = new LegacyConversion() {
		@Override
		public void convert(String tag, Meta meta) {
			if (notGiven(meta.getType())) meta.setType("MM.typeEnumSkeletal");
		}
	};

	private static LegacyConversion LEGACY_LIFESTAGE = new LegacyConversion() {
		@Override
		public void convert(String tag, Meta meta) {
			if (tag.equals("adult")) meta.addLifeStage("MY.lifeStageAdult");
			if (tag.equals("larva")) meta.addLifeStage("MY.lifeStageLarva");
			if (tag.equals("nymph")) meta.addLifeStage("MY.lifeStageNymph");
			if (tag.equals("egg")) meta.addLifeStage("MY.lifeStageEgg");
			if (tag.equals("pupa")) meta.addLifeStage("MY.lifeStagePupa");
		}
	};

	private static LegacyConversion LEGACY_SEX = new LegacyConversion() {
		@Override
		public void convert(String tag, Meta meta) {
			if (tag.equals("male")) meta.addSex("MY.sexM");
			if (tag.equals("female")) meta.addSex("MY.sexF");
		}
	};

	private static LegacyConversion LEGACY_PRIMARY = new LegacyConversion() {
		@Override
		public void convert(String tag, Meta meta) {
			if (meta.getIdentifications() == null) return;
			if (!meta.getPrimaryForTaxon().isEmpty()) return;
			for (String taxonId : meta.getIdentifications().getTaxonIds()) {
				meta.addPrimaryForTaxon(taxonId);
			}
		}
	};

	private static final Map<String, LegacyConversion> LEGACY_CONVERSIONS;
	static {
		LEGACY_CONVERSIONS = new HashMap<>();
		LEGACY_CONVERSIONS.put("primary", LEGACY_PRIMARY);
		LEGACY_CONVERSIONS.put("specimen", LEGACY_SPECIMEN);
		LEGACY_CONVERSIONS.put("label", LEGACY_LABEL);
		LEGACY_CONVERSIONS.put("live", LEGACY_LIVE);
		LEGACY_CONVERSIONS.put("habitaatti", LEGACY_HABITAT);
		LEGACY_CONVERSIONS.put("carcass", LEGACY_CARCASS);
		LEGACY_CONVERSIONS.put("adult", LEGACY_LIFESTAGE);
		LEGACY_CONVERSIONS.put("larva", LEGACY_LIFESTAGE);
		LEGACY_CONVERSIONS.put("nymph", LEGACY_LIFESTAGE);
		LEGACY_CONVERSIONS.put("egg", LEGACY_LIFESTAGE);
		LEGACY_CONVERSIONS.put("pupa", LEGACY_LIFESTAGE);
		LEGACY_CONVERSIONS.put("morfologia", LEGACY_MICROSCOPY);
		LEGACY_CONVERSIONS.put("anatomia", LEGACY_MICROSCOPY);
		LEGACY_CONVERSIONS.put("skeletal", LEGACY_SKELETAL);
		LEGACY_CONVERSIONS.put("male", LEGACY_SEX);
		LEGACY_CONVERSIONS.put("female", LEGACY_SEX);
	}

	private static boolean notGiven(String s) {
		return s == null || s.isEmpty();
	}

	public Meta doLegacyConversions() {
		for (String tag : getTags()) {
			doLegacyConversions(tag);
		}
		return this;
	}

	private void doLegacyConversions(String tag) {
		if (tag == null) return;
		tag = tag.toLowerCase();
		LegacyConversion conversion = LEGACY_CONVERSIONS.get(tag);
		if (conversion != null) {
			conversion.convert(tag, this);
		}
	}

}
