package fi.luomus.kuvapalvelu.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;

import fi.luomus.utils.model.LocalizedString;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME,
include = As.PROPERTY, property = "type") @JsonSubTypes({
	@JsonSubTypes.Type(value = License.class, name = "license"),
	@JsonSubTypes.Type(value = LifeStage.class, name = "lifeStage"),
	@JsonSubTypes.Type(value = Sex.class, name = "sex"),
	@JsonSubTypes.Type(value = Side.class, name = "side"),
	@JsonSubTypes.Type(value = Type.class, name = "type")
})
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public abstract class NamedResource {

	private String id;
	private LocalizedString label;

	public NamedResource() {}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public LocalizedString getLabel() {
		return label;
	}

	public void setLabel(LocalizedString label) {
		this.label = label;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		NamedResource other = (NamedResource) o;

		if (id != null ? !id.equals(other.id) : other.id != null) return false;
		if (label != null ? !label.equals(other.label) : other.label != null) return false;

		return true;
	}

	@Override
	public int hashCode() {
		int result = id != null ? id.hashCode() : 0;
		result = 31 * result + (label != null ? label.hashCode() : 0);
		return result;
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + " [id=" + id + ", label=" + label + "]";
	}

}
