package fi.luomus.kuvapalvelu.model;

public enum MediaClass {

	IMAGE("MM.image", "images"),
	AUDIO("MM.audio", "audio"),
	VIDEO("MM.video", "video"),
	MODEL("MM.model", "3d-model"),
	PDF("MM.pdf", "pdf");

	private final String className;
	private final String apiPath;

	private MediaClass(String className, String apiPath) {
		this.className = className;
		this.apiPath = apiPath;
	}

	public String getClassName() {
		return className;
	}

	public String getApiPath() {
		return apiPath;
	}

}