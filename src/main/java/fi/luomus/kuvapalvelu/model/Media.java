package fi.luomus.kuvapalvelu.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Media {

	private String id;
	private String secretKey;
	private Urls urls;
	private Meta meta;

	/**
	 * @return Unique identifier for the media resource
	 */
	public String getId() {
		return id;
	}

	public Media setId(String id) {
		this.id = id;
		return this;
	}

	/**
	 * @return If present, can be used to retrieve protected media resource files
	 */
	public String getSecretKey() {
		return secretKey;
	}

	public Media setSecretKey(String secretKey) {
		this.secretKey = secretKey;
		return this;
	}

	/**
	 * @return Locations of the media files
	 */
	public Urls getUrls() {
		if (urls == null) setUrls(new Urls());
		return urls;
	}

	public Media setUrls(Urls urls) {
		this.urls = urls;
		return this;
	}

	/**
	 * @return Metadata of the media
	 */
	public Meta getMeta() {
		if (meta == null) setMeta(new Meta());
		return meta;
	}

	public Media setMeta(Meta meta) {
		this.meta = meta;
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Media media = (Media) o;

		if (id != null ? !id.equals(media.id) : media.id != null) return false;
		if (meta != null ? !meta.equals(media.meta) : media.meta != null) return false;
		if (secretKey != null ? !secretKey.equals(media.secretKey) : media.secretKey != null) return false;
		if (urls != null ? !urls.equals(media.urls) : media.urls != null) return false;

		return true;
	}

	@Override
	public int hashCode() {
		int result = id != null ? id.hashCode() : 0;
		result = 31 * result + (secretKey != null ? secretKey.hashCode() : 0);
		result = 31 * result + (urls != null ? urls.hashCode() : 0);
		result = 31 * result + (meta != null ? meta.hashCode() : 0);
		return result;
	}

	@Override
	public String toString() {
		return "Media [id=" + id + ", secretKey=" + secretKey + ", urls=" + urls + ", meta=" + meta + "]";
	}

}
