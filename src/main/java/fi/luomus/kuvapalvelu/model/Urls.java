package fi.luomus.kuvapalvelu.model;

import java.net.URI;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Urls {

	@Predicate(Constants.ORIGINAL_IMAGE_URL)
	public URI original;

	@Predicate(value=Constants.FULL_IMAGE_URL, isNullable=false)
	public URI full;

	@Predicate(value=Constants.LARGE_IMAGE_URL, isNullable=false)
	public URI large;

	@Predicate(value=Constants.SQUARE_THUMBNAIL_IMAGE_URL, isNullable=false)
	public URI square;

	@Predicate(value=Constants.THUMBNAIL_IMAGE_URL, isNullable=false)
	public URI thumbnail;

	@Predicate(Constants.WAV_URL)
	public URI wav;

	@Predicate(Constants.MP3_URL)
	public URI mp3;

	@Predicate(Constants.FLAC_URL)
	public URI flac;

	@Predicate(Constants.VIDEO_URL)
	public URI video;

	@Predicate(Constants.LOW_DETAIL_MODEL_URL)
	public URI lowDetailModel;

	@Predicate(Constants.HIGH_DETAIL_MODEL_URL)
	public URI highDetailModel;

	@Predicate(Constants.PDF_URL)
	public URI pdf;

	/**
	 * @return The originally uploaded file
	 */
	public URI getOriginal() {
		return original;
	}

	public Urls setOriginal(URI original) {
		this.original = original;
		return this;
	}

	/**
	 * @return Full-sized but reasonably compressed version of the original file (or spectrogram of uploaded audio file)
	 */
	public URI getFull() {
		return full;
	}

	public Urls setFull(URI full) {
		this.full = full;
		return this;
	}

	/**
	 * @return A large but limited size version of the original file
	 */
	public URI getLarge() {
		return large;
	}

	public Urls setLarge(URI large) {
		this.large = large;
		return this;
	}

	/**
	 * @return Medium-sized 1:1 cropped version of the original file
	 */
	public URI getSquare() {
		return square;
	}

	public Urls setSquare(URI square) {
		this.square = square;
		return this;
	}

	/**
	 * @return Thumbnail-sized version of the original file (or spectrogram of the audio file)
	 */
	public URI getThumbnail() {
		return thumbnail;
	}

	public Urls setThumbnail(URI thumbnail) {
		this.thumbnail = thumbnail;
		return this;
	}

	/**
	 * @return Wav version of original audio file (if original file was wav)
	 */
	public URI getWav() {
		return wav;
	}

	public Urls setWav(URI wav) {
		this.wav = wav;
		return this;
	}

	/**
	 * @return Mp3 version of original audio file
	 */
	public URI getMp3() {
		return mp3;
	}

	public Urls setMp3(URI mp3) {
		this.mp3 = mp3;
		return this;
	}

	/**
	 * @return Flac version of original audio file (if original file was flac)
	 */
	public URI getFlac() {
		return flac;
	}

	public Urls setFlac(URI flac) {
		this.flac = flac;
		return this;
	}

	public URI getVideo() {
		return video;
	}

	public Urls setVideo(URI video) {
		this.video = video;
		return this;
	}

	public URI getLowDetailModel() {
		return lowDetailModel;
	}

	public Urls setLowDetailModel(URI lowDetailModel) {
		this.lowDetailModel = lowDetailModel;
		return this;
	}

	public URI getHighDetailModel() {
		return highDetailModel;
	}

	public Urls setHighDetailModel(URI highDetailModel) {
		this.highDetailModel = highDetailModel;
		return this;
	}

	public URI getPdf() {
		return pdf;
	}

	public Urls setPdf(URI pdf) {
		this.pdf = pdf;
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Urls urls = (Urls) o;

		if (full != null ? !full.equals(urls.full) : urls.full != null) return false;
		if (large != null ? !large.equals(urls.large) : urls.large != null) return false;
		if (original != null ? !original.equals(urls.original) : urls.original != null) return false;
		if (square != null ? !square.equals(urls.square) : urls.square != null) return false;
		if (thumbnail != null ? !thumbnail.equals(urls.thumbnail) : urls.thumbnail != null) return false;
		if (wav != null ? !wav.equals(urls.wav) : urls.wav != null) return false;
		if (mp3 != null ? !mp3.equals(urls.mp3) : urls.mp3 != null) return false;
		if (flac != null ? !flac.equals(urls.flac) : urls.flac != null) return false;
		if (video != null ? !video.equals(urls.video) : urls.video != null) return false;
		if (lowDetailModel != null ? !lowDetailModel.equals(urls.lowDetailModel) : urls.lowDetailModel != null) return false;
		if (highDetailModel != null ? !highDetailModel.equals(urls.highDetailModel) : urls.highDetailModel != null) return false;
		if (pdf != null ? !pdf.equals(urls.pdf) : urls.pdf != null) return false;
		return true;
	}

	@Override
	public int hashCode() {
		int result = original != null ? original.hashCode() : 0;
		result = 31 * result + (full != null ? full.hashCode() : 0);
		result = 31 * result + (large != null ? large.hashCode() : 0);
		result = 31 * result + (square != null ? square.hashCode() : 0);
		result = 31 * result + (thumbnail != null ? thumbnail.hashCode() : 0);
		result = 31 * result + (wav != null ? wav.hashCode() : 0);
		result = 31 * result + (mp3 != null ? mp3.hashCode() : 0);
		result = 31 * result + (flac != null ? flac.hashCode() : 0);
		result = 31 * result + (video != null ? video.hashCode() : 0);
		result = 31 * result + (lowDetailModel != null ? lowDetailModel.hashCode() : 0);
		result = 31 * result + (highDetailModel != null ? highDetailModel.hashCode() : 0);
		result = 31 * result + (pdf != null ? pdf.hashCode() : 0);
		return result;
	}

	@Override
	public String toString() {
		return "Urls [original=" + original + ", full=" + full + ", large=" + large + ", square=" + square + ", thumbnail=" + thumbnail + ", wav=" + wav + ", mp3=" + mp3
				+ ", flac=" + flac + ", video=" + video + ", lowDetailModel=" + lowDetailModel + ", highDetailModel=" + highDetailModel + ", pdf= " + pdf + "]";
	}

}
