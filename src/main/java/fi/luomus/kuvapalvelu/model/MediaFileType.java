package fi.luomus.kuvapalvelu.model;

public enum MediaFileType {

	ORIGINAL_IMAGE
	(Constants.ORIGINAL_IMAGE_URL, MediaClass.IMAGE),

	FULL_IMAGE
	(Constants.FULL_IMAGE_URL, MediaClass.IMAGE),

	LARGE_IMAGE
	(Constants.LARGE_IMAGE_URL, MediaClass.IMAGE),

	THUMBNAIL_IMAGE
	(Constants.THUMBNAIL_IMAGE_URL, MediaClass.IMAGE),

	SQUARE_THUMBNAIL_IMAGE
	(Constants.SQUARE_THUMBNAIL_IMAGE_URL, MediaClass.IMAGE),

	WAV
	(Constants.WAV_URL, MediaClass.AUDIO),

	MP3
	(Constants.MP3_URL, MediaClass.AUDIO),

	FLAC
	(Constants.FLAC_URL, MediaClass.AUDIO),

	VIDEO
	(Constants.VIDEO_URL, MediaClass.VIDEO),

	LOW_DETAIL_MODEL
	(Constants.LOW_DETAIL_MODEL_URL, MediaClass.MODEL),

	HIGH_DETAIL_MODEL
	(Constants.HIGH_DETAIL_MODEL_URL, MediaClass.MODEL),

	PDF
	(Constants.PDF_URL, MediaClass.PDF);

	private final String predicateName;
	private final MediaClass mediaClass;

	private MediaFileType(String predicateName, MediaClass mediaClass) {
		this.predicateName = predicateName;
		this.mediaClass = mediaClass;
	}

	public String getPredicateName() {
		return predicateName;
	}

	public MediaClass getMediaClass() {
		return mediaClass;
	}

}