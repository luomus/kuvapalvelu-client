package fi.luomus.kuvapalvelu.model;

public class Constants {

	public static final String ORIGINAL_IMAGE_URL = "MM.originalURL";

	public static final String FULL_IMAGE_URL = "MM.fullURL";

	public static final String LARGE_IMAGE_URL = "MM.largeURL";

	public static final String THUMBNAIL_IMAGE_URL = "MM.thumbnailURL";

	public static final String SQUARE_THUMBNAIL_IMAGE_URL = "MM.squareThumbnailURL";

	public static final String WAV_URL = "MM.wavURL";

	public static final String MP3_URL = "MM.mp3URL";

	public static final String FLAC_URL = "MM.flacURL";

	public static final String VIDEO_URL = "MM.videoURL";

	public static final String LOW_DETAIL_MODEL_URL = "MM.lowDetailModelURL";

	public static final String HIGH_DETAIL_MODEL_URL = "MM.highDetailModelURL";

	public static final String PDF_URL = "MM.pdfURL";

}
