package fi.luomus.kuvapalvelu.model;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class NewMultiSourceMedia implements Iterable<NewMultiSourceMedia.MediaFile> {

	@JsonIgnoreProperties(ignoreUnknown = true)
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	public static class MediaFile {
		private String tempFileId;
		private MediaFileType type;
		private String filename;
		public MediaFile() {}
		public MediaFile(String tempFileId, MediaFileType type, String filename) {
			this.tempFileId = tempFileId;
			this.type = type;
			this.filename = filename;
		}
		public String getTempFileId() {
			return tempFileId;
		}
		public MediaFileType getType() {
			return type;
		}
		public String getFilename() {
			return filename;
		}
		public void setFilename(String filename) {
			this.filename = filename;
		}
		public void setTempFileId(String tempFileId) {
			this.tempFileId = tempFileId;
		}
		public void setType(MediaFileType type) {
			this.type = type;
		}
		@Override
		public String toString() {
			return "MediaFile [tempFileId=" + tempFileId + ", type=" + type + ", filename=" + filename + "]";
		}
	}

	private Meta meta;
	private MediaClass mediaClass;
	private List<MediaFile> entries;

	public Meta getMeta() {
		return meta;
	}

	public NewMultiSourceMedia setMeta(Meta meta) {
		this.meta = meta;
		return this;
	}

	public MediaClass getMediaClass() {
		return mediaClass;
	}

	public NewMultiSourceMedia setMediaClass(MediaClass mediaClass) {
		this.mediaClass = mediaClass;
		return this;
	}

	public List<MediaFile> getEntries() {
		if (entries == null) setEntries(new ArrayList<>());
		return entries;
	}

	public NewMultiSourceMedia setEntries(List<MediaFile> entries) {
		this.entries = entries;
		return this;
	}

	@Override
	public Iterator<MediaFile> iterator() {
		return getEntries().iterator();
	}

	@Override
	public String toString() {
		return "NewMultiSourceMedia [meta=" + meta + ", mediaClass=" + mediaClass + ", entries=" + entries + "]";
	}

}
