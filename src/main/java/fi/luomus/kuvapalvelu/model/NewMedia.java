package fi.luomus.kuvapalvelu.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class NewMedia {

	private String tempFileId;
	private Meta meta;
	private MediaClass mediaClass;

	public Meta getMeta() {
		return meta;
	}

	public NewMedia setMeta(Meta meta) {
		this.meta = meta;
		return this;
	}

	public String getTempFileId() {
		return tempFileId;
	}

	public NewMedia setTempFileId(String tempFileId) {
		this.tempFileId = tempFileId;
		return this;
	}

	public MediaClass getMediaClass() {
		return mediaClass;
	}

	public NewMedia setMediaClass(MediaClass mediaClass) {
		this.mediaClass = mediaClass;
		return this;
	}

	@Override
	public String toString() {
		return "NewMedia [tempFileId=" + tempFileId + ", meta=" + meta + ", mediaClass=" + mediaClass + "]";
	}

}
