# Media-API client

This client for Java can be used to connect to [Media API](https://bitbucket.org/luomus/kuvapalvelu-server/). 

## Install

```
$ git clone git@bitbucket.org:USERNAME/kuvapalvelu-client.git
$ cd kuvapalvelu-client
$ mvn install
```

## Usage
```
MediaApiClient withBuilderSyntax = MediaApiClientImpl.builder()
        .uri("https://foo.bar/kuvapalvelu")
        .username("user")
        .password("password")
        .build();

Note that the path doesn't include /api/ and there is no trailing /

MediaApiClient withInterfaceImplementation = new MediaApiClientImpl(new MediaApiPreferences() {
    @Override
    public URI getBaseUri() {
        return IMAGE_API_ADDRESS;
    }

    @Override
    public String getUsername() {
        return "username";
    }

    @Override
    public String getPassword() {
        return "password";
    }
});
```
```
Meta meta = new Meta();
...
// Upload an image
String uploadedId = MediaApiClient.uploadImage(new File("image.jpeg"), meta);
// fetch image meta and other data
Optional<Media> mediaOptional = mediaApiClient.get(MediaClass.IMAGE, uploadedId);
System.out.print("uploaded at: " + mediaOptional.get().getMeta().getUploadDateTime());
// Delete
mediaApiClient.delete(MediaClass.IMAGE, uploadedId);

// Fetch all taxon MX.123 images
List<Media> images = mediaApiClient.search(MediaClass.IMAGE, "identification.taxonIds", "MX.123");
```

Read more from Media-API server documentation.

